//META{"name":"PinPluginsAndThemes","source":"https://gitlab.com/_Lighty_/bdstuff/blob/master/PinPluginsAndThemes.plugin.js","website":"https://_lighty_.gitlab.io/bdstuff/?plugin=PinPluginsAndThemes"}*//
/*@cc_on
@if (@_jscript)

	// Offer to self-install for clueless users that try to run this directly.
	var shell = WScript.CreateObject('WScript.Shell');
	var fs = new ActiveXObject('Scripting.FileSystemObject');
	var pathPlugins = shell.ExpandEnvironmentStrings('%APPDATA%\\BetterDiscord\\plugins');
	var pathSelf = WScript.ScriptFullName;
	// Put the user at ease by addressing them in the first person
	shell.Popup('It looks like you\'ve mistakenly tried to run me directly. \n(Don\'t do that!)', 0, 'I\'m a plugin for BetterDiscord', 0x30);
	if (fs.GetParentFolderName(pathSelf) === fs.GetAbsolutePathName(pathPlugins)) {
		shell.Popup('I\'m in the correct folder already.\nJust reload Discord with Ctrl+R.', 0, 'I\'m already installed', 0x40);
	} else if (!fs.FolderExists(pathPlugins)) {
		shell.Popup('I can\'t find the BetterDiscord plugins folder.\nAre you sure it\'s even installed?', 0, 'Can\'t install myself', 0x10);
	} else if (shell.Popup('Should I copy myself to BetterDiscord\'s plugins folder for you?', 0, 'Do you need some help?', 0x34) === 6) {
		fs.CopyFile(pathSelf, fs.BuildPath(pathPlugins, fs.GetFileName(pathSelf)), true);
		// Show the user where to put plugins in the future
		shell.Exec('explorer ' + pathPlugins);
		shell.Popup('I\'m installed!\nJust reload Discord with Ctrl+R.', 0, 'Successfully installed', 0x40);
	}
	WScript.Quit();

@else@*/

var PinPluginsAndThemes = (() => {
  /* Setup */
  const config = {
    main: 'index.js',
    info: { name: 'PinPluginsAndThemes', authors: [{ name: 'Lighty', discord_id: '239513071272329217', github_username: 'LightyPon', twitter_username: '' }], version: '1.0.2', description: 'Right click on a plugins header or click the button to pin it to the top! Works with RepoControls by Devilbro. to change the color, use custom css:\n:root {\n      --PPAT-color: rgb(142, 112, 216);\n    }', github: 'https://gitlab.com/_Lighty_', github_raw: 'https://_lighty_.gitlab.io/bdstuff/plugins/PinPluginsAndThemes.plugin.js' },
    changelog: [{
      title: 'sad',
      type: 'added',
      items: ['Fixed crash if XenoLib or ZeresPluginLib were missing']
    }]
  };

  /* Build */
  const buildPlugin = ([Plugin, Api]) => {
    const { Utilities, WebpackModules, DiscordModules, ReactTools, Patcher, PluginUtilities, Logger, Toasts } = Api;
    const { React, ContextMenuItemsGroup, ContextMenuActions, ContextMenuItem } = DiscordModules;
    const joinClassNames = WebpackModules.getModule(e => e.default && e.default.default);
    const Tooltip = WebpackModules.getByDisplayName('Tooltip');
    const Icon = WebpackModules.getByDisplayName('Icon');

    const GetClass = arg => {
      const args = arg.split(' ');
      return WebpackModules.getByProps(...args)[args[args.length - 1]];
    };

    const iconWrapper = GetClass('divider iconWrapper');
    const clickable = GetClass('divider clickable');
    const iconClass = GetClass('toolbar icon');
    const iconSelected = GetClass('iconWrapper selected');

    class PinContextMenu extends React.PureComponent {
      render() {
        return React.createElement(
          'div',
          {
            type: 'PLUGIN_HEADER',
            className: PinContextMenu.ConstClasses.ContextMenu
          },
          React.createElement(
            ContextMenuItemsGroup,
            {},
            React.createElement(ContextMenuItem, {
              label: this.props.pinned ? 'Unpin' : 'Pin',
              action: () => {
                ContextMenuActions.closeContextMenu();
                this.props.pinToggle();
              }
            })
          )
        );
      }
    }

    PinContextMenu.displayName = 'PPATContextMenu';
    PinContextMenu.ConstClasses = {
      ContextMenu: GetClass('contextMenu')
    };

    return class PinPluginsAndThemes extends Plugin {
      constructor() {
        super();
        this.patchTarget = this.patchTarget.bind(this);
      }
      onStart() {
        this.data = PluginUtilities.loadData(this.name, 'data', {
          pinned: []
        });

        PluginUtilities.addStyle(
          this.short,
          `
          :root {
            --PPAT-color: rgb(142, 112, 216);
          }

          .bda-slist > li.PPAT-pinned {
            border: 1px solid var(--PPAT-color) !important;
          }

          .PPAT-border {
            background: var(--PPAT-color);
            display: block;
            width: 100%;
            height: 10px;
            margin-bottom: 20px;
          }
          `
        );
        /* setttimeout because repocontrols is a disabled plugin, or maybe devilbros library is idk */
        this.timeout = setTimeout(() => {
          this.patchAll();
          this.updateLists();
        }, 1000);
      }
      onStop() {
        clearTimeout(this.timeout);
        PluginUtilities.removeStyle(this.short);
        Patcher.unpatchAll();
        this.updateLists();
      }

      updateLists() {
        const owner = ReactTools.getOwnerInstance(document.querySelector('.bda-slist'));
        if (owner) {
          const old = document.querySelector('.content-region-scroller').scrollTop;
          owner.forceUpdate();
          /* restore scroll */
          setImmediate(() => (document.querySelector('.content-region-scroller').scrollTop = old));
        }
        const items = document.querySelectorAll('.bda-slist > .ui-switch-item');
        items.forEach(e => {
          const own = ReactTools.getOwnerInstance(e);
          own.forceUpdate();
        });
      }

      togglePin(name) {
        if (this.data.pinned.indexOf(name) !== -1) {
          this.data.pinned.splice(this.data.pinned.indexOf(name), 1);
          Toasts.success('Unpinned!');
        } else {
          this.data.pinned.push(name);
          Toasts.success('Pinned!');
        }
        this.updateLists();
		PluginUtilities.saveData(this.name, 'data', this.data);
      }

      /* patches */

      patchAll() {
        this.patchV2C_List();
        this.patchCards();
      }

      patchV2C_List() {
        Patcher.after(V2C_List.prototype, 'render', (_this, args, ret) => {
          const arrays = [[], []];
          ret.props.children.forEach(e => (this.data.pinned.indexOf(e.key) !== -1 && arrays[0].push(e)) || arrays[1].push(e));
          ret.props.children = [
            ...arrays[0],
            arrays[0].length && arrays[1].length
              ? React.createElement('div', {
                  className: 'PPAT-border'
                })
              : false,
            ...arrays[1]
          ];
        });
      }

      patchCards() {
        /* can't use after because it causes bugs in other plugins */
        Patcher.after(V2C_PluginCard.prototype, 'render', this.patchTarget);
        Patcher.after(V2C_ThemeCard.prototype, 'render', this.patchTarget);
      }

      patchTarget(_, __, ret) {
        const controlsProps = Utilities.getNestedProp(ret, 'props.children.0.props.children.1.props');
        const headerProps = Utilities.getNestedProp(ret, 'props.children.0.props');
        if (!controlsProps || !headerProps) return ret;
        const name = ret.props['data-name'];
        if (this.data.pinned.indexOf(name) !== -1) ret.props.className += ' PPAT-pinned';
        headerProps.onContextMenu = e =>
          ContextMenuActions.openContextMenu(e, e => {
            return React.createElement(PinContextMenu, {
              onHeightUpdate: () => {}, // don't cause errors in ExtendedContextMenu plugin, also why it's a class
              pinToggle: () => this.togglePin(name),
              pinned: this.data.pinned.indexOf(name) !== -1
            });
          });
        controlsProps.children.unshift(this.createPinIcon(name));
        return ret;
      }

      createPinIcon(name) {
        return React.createElement(
          Tooltip,
          {
            text: 'Pin',
            position: 'bottom',
            hideOnClick: true
          },
          t =>
            React.createElement(
              'div',
              {
                ...t,
                className: joinClassNames(iconWrapper, clickable, this.data.pinned.indexOf(name) !== -1 ? iconSelected : ''),
                role: 'button',
                onClick: e => this.togglePin(name),
                style: {
                  marginLeft: 0,
                  marginRight: 5
                }
              },
              React.createElement(Icon, {
                name: 'Nova_Pin',
                className: iconClass
              })
            )
        );
      }

      /* patches end */

      get [Symbol.toStringTag]() {
        return 'Plugin';
      }
      get css() {
        return this._css;
      }
      get name() {
        return config.info.name;
      }
      get short() {
        let string = '';

        for (let i = 0, len = config.info.name.length; i < len; i++) {
          const char = config.info.name[i];
          if (char === char.toUpperCase()) string += char;
        }

        return string;
      }
      get author() {
        return config.info.authors.map(author => author.name).join(', ');
      }
      get version() {
        return config.info.version;
      }
      get description() {
        return config.info.description;
      }
    };
  };

  /* Finalize */

  return !global.ZeresPluginLibrary
    ? class {
        getName() {
          return this.name.replace(/\s+/g, '');
        }

        getAuthor() {
          return this.author;
        }

        getVersion() {
          return this.version;
        }

        getDescription() {
          return this.description;
        }

        stop() {}

        load() {
          const title = 'Library Missing';
          const ModalStack = window.BdApi.findModuleByProps('push', 'update', 'pop', 'popWithKey');
          const TextElement = window.BdApi.findModuleByProps('Sizes', 'Weights');
          const ConfirmationModal = window.BdApi.findModule(m => m.defaultProps && m.key && m.key() === 'confirm-modal');
          if (!ModalStack || !ConfirmationModal || !TextElement) return window.BdApi.getCore().alert(title, `The library plugin needed for ${config.info.name} is missing.<br /><br /> <a href="https://betterdiscord.net/ghdl?url=https://raw.githubusercontent.com/rauenzi/BDPluginLibrary/master/release/0PluginLibrary.plugin.js" target="_blank">Click here to download the library!</a>`);
          ModalStack.push(function(props) {
            return window.BdApi.React.createElement(
              ConfirmationModal,
              Object.assign(
                {
                  header: title,
                  children: [
                    BdApi.React.createElement(TextElement, {
                      color: TextElement.Colors.PRIMARY,
                      children: [`The library plugin needed for ${config.info.name} is missing. Please click Download Now to install it.`]
                    })
                  ],
                  red: false,
                  confirmText: 'Download Now',
                  cancelText: 'Cancel',
                  onConfirm: () => {
                    require('request').get('https://rauenzi.github.io/BDPluginLibrary/release/0PluginLibrary.plugin.js', async (error, response, body) => {
                      if (error) return require('electron').shell.openExternal('https://betterdiscord.net/ghdl?url=https://raw.githubusercontent.com/rauenzi/BDPluginLibrary/master/release/0PluginLibrary.plugin.js');
                      await new Promise(r => require('fs').writeFile(require('path').join(window.ContentManager.pluginsFolder, '0PluginLibrary.plugin.js'), body, r));
                    });
                  }
                },
                props
              )
            );
          });
        }

        start() {}
        get [Symbol.toStringTag]() {
          return 'Plugin';
        }
        get name() {
          return config.info.name;
        }
        get short() {
          let string = '';
          for (let i = 0, len = config.info.name.length; i < len; i++) {
            const char = config.info.name[i];
            if (char === char.toUpperCase()) string += char;
          }
          return string;
        }
        get author() {
          return config.info.authors.map(author => author.name).join(', ');
        }
        get version() {
          return config.info.version;
        }
        get description() {
          return config.info.description;
        }
      }
    : buildPlugin(global.ZeresPluginLibrary.buildPlugin(config));
})();

/*@end@*/
